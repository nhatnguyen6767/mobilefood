//
//  DriverOrderCell.swift
//  MobileFood
//
//  Created by net=0 on 4/10/20.
//

import UIKit

class DriverOrderCell: UITableViewCell {

    @IBOutlet weak var lbRestaurantName: UILabel!
    
    @IBOutlet weak var lbCustomerName: UILabel!
    
    @IBOutlet weak var lbCustomerAddress: UILabel!
    
    @IBOutlet weak var imgCustomerAvatar: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

}
