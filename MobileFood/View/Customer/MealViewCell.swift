//
//  MealViewCell.swift
//  MobileFood
//
//  Created by net=0 on 23/9/20.
//

import UIKit

class MealViewCell: UITableViewCell {

    
    @IBOutlet weak var lbMealName: UILabel!
    @IBOutlet weak var lbMealShortDescription: UILabel!
    @IBOutlet weak var lbMealPrice: UILabel!
    @IBOutlet weak var imgMealImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

}
