//
//  RestaurantViewCell.swift
//  MobileFood
//
//  Created by net=0 on 23/9/20.
//

import UIKit

class RestaurantViewCell: UITableViewCell {

    @IBOutlet weak var lbRestaurantName: UILabel!
    
    @IBOutlet weak var lbRestaurantAddress: UILabel!
    @IBOutlet weak var imgRestaurantLogo: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

}
