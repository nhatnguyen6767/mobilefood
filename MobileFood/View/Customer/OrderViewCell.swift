//
//  OrderViewCell.swift
//  MobileFood
//
//  Created by net=0 on 26/9/20.
//

import UIKit

class OrderViewCell: UITableViewCell {

    @IBOutlet weak var lbQty: UILabel!
    @IBOutlet weak var lbMealName: UILabel!
    @IBOutlet weak var lbSubTotal: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

}
