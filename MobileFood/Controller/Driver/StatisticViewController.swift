//
//  StatisticViewController.swift
//  MobileFood
//
//  Created by net=0 on 4/10/20.
//

import UIKit
import Charts

class StatisticViewController: UIViewController {

    @IBOutlet weak var menuBarButton: UIBarButtonItem!
    
    @IBOutlet weak var viewChart: BarChartView!
    
    var weekdays = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"]
    override func viewDidLoad() {
        super.viewDidLoad()

        if self.revealViewController() != nil {
            menuBarButton.target = self.revealViewController()
            menuBarButton.action = #selector(SWRevealViewController.revealToggle(animated:))
            self.view.addGestureRecognizer((self.revealViewController()?.panGestureRecognizer())!)
        }
        
        // #1 Initialize chart
        self.initilizeChart()
        
        // #2 Load data to chart
        self.loadDataToChart()
        
    }
    
    func initilizeChart() {
        
        viewChart.noDataText = "No Data"
        viewChart.animate(xAxisDuration: 2.0, yAxisDuration: 2.0, easingOption: .easeInBounce)
        viewChart.xAxis.labelPosition = .bottom
        viewChart.chartDescription?.enabled = true
        viewChart.chartDescription?.text = ""
        viewChart.xAxis.setLabelCount(0, force: true)
        
        viewChart.legend.enabled = false
        viewChart.scaleYEnabled = false
        viewChart.scaleXEnabled = false
        viewChart.pinchZoomEnabled = false
        viewChart.doubleTapToZoomEnabled = false
        viewChart.leftAxis.axisMinimum = 0.0
        viewChart.leftAxis.axisMaximum = 100.00
        
        viewChart.highlighter = nil
        viewChart.rightAxis.enabled = false
        viewChart.xAxis.drawGridLinesEnabled = false
        
        
    }
    
    func loadDataToChart() {
        
        APIManager.shared.getDriverRevenue { (json) in
            print(json)
            
            if json != nil {
                
                let revenue = json["revenue"]
                
                var dataEntries: [BarChartDataEntry] = []
                
                for i in 0..<self.weekdays.count {
                    let day = self.weekdays[i]
                    let dataEntry = BarChartDataEntry(x: revenue[day].double!, y: Double(i))
                    dataEntries.append(dataEntry)
                    
                }
                
                let chartDataSet = BarChartDataSet(entries: dataEntries, label: "Revenue by day")
                chartDataSet.colors = ChartColorTemplates.material()
                
                let chartData = BarChartData(dataSet: chartDataSet)
                
                self.viewChart.data = chartData
                
            }
            
        }
    }
    
}
