//
//  PaymentViewController.swift
//  MobileFood
//
//  Created by net=0 on 21/9/20.
//

import UIKit
import Stripe

class PaymentViewController: UIViewController {
    
    @IBOutlet weak var cardTextField: STPPaymentCardTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    @IBAction func placeOrder(_ sender: Any) {
        APIManager.shared.getLatestOrder { (json) in
            
            if !(json["order"]["status"] == nil || json["order"]["status"] == "Delivered") {
                // Processing the payment and create an Order
                
                //                let card = self.cardTextField.cardParams
                
                let cardParams: STPCardParams = STPCardParams()
                cardParams.number = self.cardTextField!.cardNumber
                cardParams.expMonth = self.cardTextField!.expirationMonth
                cardParams.expYear = self.cardTextField!.expirationYear
                cardParams.cvc = self.cardTextField!.cvc
                
                STPAPIClient.shared().createToken(withCard: cardParams) { (token, error) in
                    if let myError = error {
                        print("Error:", myError)
                    } else if let stripeToken = token {
                        
                        APIManager.shared.createOrder(stripeToken: stripeToken.tokenId) { (json) in
                            
                            Tray.currentTray.reset()
                            self.performSegue(withIdentifier: "ViewOrder", sender: self)
                        }
                    }
                    
                }
            } else {
                // Showing an alert message.
                
                let cancelAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil)
                
                let okAction = UIAlertAction(title: "Go to order", style: UIAlertAction.Style.default) { (action) in
                    self.performSegue(withIdentifier: "ViewOrder", sender: self)
                }
                
                let alertView = UIAlertController(title: "Already Order?", message: "Your current order isn't completed", preferredStyle: UIAlertController.Style.alert)
                
                alertView.addAction(okAction)
                alertView.addAction(cancelAction)
                
                self.present(alertView, animated: true, completion: nil)
                
            }
        }
    }
    
}
