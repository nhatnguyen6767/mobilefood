//
//  MealListTableViewController.swift
//  MobileFood
//
//  Created by net=0 on 20/9/20.
//

import UIKit

class MealListTableViewController: UITableViewController {

    var restaurant: Restaurant?
    var meals = [Meal]()
    
    let activityIndicator = UIActivityIndicatorView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let restaurantName = restaurant?.name {
            self.navigationItem.title = restaurantName
        }
        
        loadMeals()
        
    }
    
    func loadMeals() {
        
        Helpers.showActivityIndicator(activityIndicator, view)
        if let restaurantId = restaurant?.id {
            APIManager.shared.getMeals(restaurantId: restaurantId) { (json) in
                
                if json != nil {
                    self.meals = []
                    if let tempMeals = json["meals"].array {
                        
                        for item in tempMeals {
                            let meal = Meal(json: item)
                            self.meals.append(meal)
                        }
                        
                        self.tableView.reloadData()
                        Helpers.hideActivityIndicator(self.activityIndicator)
                    }
                }
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "MealDetails" {
            let controller = segue.destination as! MealDetailsViewController
            controller.meal = meals[(tableView.indexPathForSelectedRow?.row)!]
            controller.restaurant = restaurant
        }
    }
    
//    func loadImage(imageView: UIImageView, urlString: String) {
//        let imgURL: URL = URL(string: urlString)!
//        URLSession.shared.dataTask(with: imgURL) { (data, response, error) in
//            guard let data = data, error == nil else {return}
//            DispatchQueue.main.async {
//                imageView.image = UIImage(data: data)
//            }
//        }.resume()
//    }
//
//    func showActivityIndicator() {
//
//        activityIndicator.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0)
//        activityIndicator.center = view.center
//        activityIndicator.hidesWhenStopped = true
//        activityIndicator.style = UIActivityIndicatorView.Style.whiteLarge
//        activityIndicator.color = UIColor.black
//
//        view.addSubview(activityIndicator)
//        activityIndicator.startAnimating()
//    }
//
//    func hideActivityIndicator() {
//
//        activityIndicator.stopAnimating()
//
//    }
    
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.meals.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MealCell", for: indexPath) as! MealViewCell
        
        let meal = meals[indexPath.row]
        cell.lbMealName.text = meal.name
        cell.lbMealShortDescription.text = meal.short_description
        
        if let price = meal.price {
            cell.lbMealPrice.text = "$\(price)"
        }
        
        if let image = meal.image {
            Helpers.loadImage(cell.imgMealImage, "\(image)")
        }
        return cell
    }

}
