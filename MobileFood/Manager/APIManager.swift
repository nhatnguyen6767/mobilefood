//
//  APIManager.swift
//  MobileFood
//
//  Created by net=0 on 22/9/20.
//  pod 'Alamofire', '~> 4.5.1'

import Foundation
import Alamofire
import SwiftyJSON
import FBSDKLoginKit
import CoreLocation

class APIManager {
    
    static let shared = APIManager()
    
    let baseURL = URL(string: BASE_URL)
    var accessToken = ""
    var refreshToken = ""
    var expired = Date()
    
    
    // API to login an user
    func login(userType: String, completionHandler: @escaping(NSError?) -> Void) {
        
        let path = "api/social/convert-token/"
        let url = baseURL!.appendingPathComponent(path)
        let params:[String: Any] = [
            "grant_type": "convert_token",
            "client_id": CLIENT_ID,
            "client_secret": CLIENT_SECRET,
            "backend": "facebook",
            "token": AccessToken.current!.tokenString,
            "user_type": userType
        ]
        
        Alamofire.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            switch response.result {
            case .success(let value):
                let jsonData = JSON(value)
                self.accessToken = jsonData["access_token"].string!
                self.refreshToken = jsonData["refresh_token"].string!
                self.expired = Date().addingTimeInterval(TimeInterval(jsonData["expires_in"].int!))
                completionHandler(nil)
                break
            case .failure(let error):
                completionHandler(error as NSError)
                break
            }
        }
        
        
    }
    
    // API to log user out
    func logout(completionHandler: @escaping(Error?) -> Void) {
        
        let path = "api/social/revoke-token/"
        let url = baseURL!.appendingPathComponent(path)
        let params: [String: Any] = [
            "client_id": CLIENT_ID,
            "client_secret": CLIENT_SECRET,
            "token": self.accessToken
        ]
        
        Alamofire.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseString { (response) in
            switch response.result {
            case .success:
                completionHandler(nil)
                break
            case .failure(let error):
                completionHandler(error as NSError?)
                break
            }
        }
        
        
        
    }
    
    // API to refresh the token when it's expired
    func refreshTokenIfNeed(completionHandler: @escaping () -> Void){
        let path = "api/social/refresh-token/"
        let url = baseURL!.appendingPathComponent(path)
        let params: [String: Any] = [
            "access_token": self.accessToken,
            "refresh_token": self.refreshToken
        ]
        
        if (Date() > self.expired) {
            Alamofire.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
                switch response.result {
                case .success(let value):
                    let jsonData = JSON(value)
                    self.accessToken = jsonData["access_token"].string!
                    self.expired = Date().addingTimeInterval(TimeInterval(jsonData["expires_in"].int!))
                    completionHandler()
                    break
                case .failure:
                    break
                }
            }
        } else {
            completionHandler()
        }
        
    }
    
    // request server function
    func requestServer(_ method: Alamofire.HTTPMethod,_ path: String,_ params: [String: Any]?,_ endcoding: ParameterEncoding,_ completionHandler: @escaping (JSON) -> Void) {
        
        let url = baseURL!.appendingPathComponent(path)
        refreshTokenIfNeed {
            Alamofire.request(url, method: method, parameters: params, encoding: endcoding, headers: nil).responseJSON { (response) in
                
                switch response.result {
                case .success(let value):
                    let jsonData = JSON(value)
                    completionHandler(jsonData)
                    break
                case .failure:
//                    completionHandler(<#JSON#>)
                    break
                }
             
            }
        }
        
    }
    
    
    /**
        CUSTOMERS
     */
    // API getting Restaurants list
    func getRestaurants(completionHandler: @escaping (JSON) ->Void){
        let path = "api/customer/restaurants/"
//        let url = baseURL!.appendingPathComponent(path)
//        refreshTokenIfNeed {
//            Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
//
//                switch response.result {
//                case .success(let value):
//                    let jsonData = JSON(value)
//                    completionHandler(jsonData)
//                    break
//                case .failure:
////                    completionHandler(nil)
//                    break
//                }
//
//
//            }
//        }
        
        requestServer(.get, path, nil, URLEncoding.default, completionHandler)
    }
    
    // API getting list of meals of restaurant.
    func getMeals(restaurantId: Int, completionHandler: @escaping (JSON) ->Void) {
        
        let path = "api/customer/meals/\(restaurantId)"
        requestServer(.get, path, nil, URLEncoding.default, completionHandler)
        
    }
    
    // API creating new order
    func createOrder(stripeToken: String, completionHandler: @escaping (JSON) -> Void) {
        
        let path = "api/customer/order/add/"
        let simpleArray = Tray.currentTray.items
        let jsonArray = simpleArray.map { (item) in
            return [
                "meal_id": item.meal.id!,
                "quantity": item.qty
            ]
        }
        
        if JSONSerialization.isValidJSONObject(jsonArray) {
            
            do {
                
                let data = try JSONSerialization.data(withJSONObject: jsonArray, options: [])
                let dataString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)!
                
                let params: [String: Any] = [
                    "access_token": self.accessToken,
                    "stripe_token": stripeToken,
                    "restaurant_id": "\(Tray.currentTray.restaurant!.id!)",
                    "order_details": dataString,
                    "address": Tray.currentTray.address!
                ]
                
                requestServer(.post, path, params, URLEncoding.default, completionHandler)
            }
            catch {
                
                print("JSON serialization failed: -->>", error.localizedDescription)
            }
        }
    }
    
    // API Getting the latest order (customer)
    func getLatestOrder(completionHandler: @escaping (JSON) -> Void) {
        
        let path = "api/customer/order/latest/"
        let params: [String: Any] = [
            "access_token": self.accessToken
        ]
        
        requestServer(.get, path, params, URLEncoding.default, completionHandler)
    }
    
    // API - Getting Driver's location
    func getDriverLocation(completionHandler: @escaping (JSON) -> Void) {
        let path = "api/customer/driver/location/"
        let params: [String: Any] = [
            "access_token" : self.accessToken
        ]
        
        requestServer(.get, path, params, URLEncoding.default, completionHandler)
    }
    
    /**
        DRIVERS
     */
    // API - Getting list of orders that are ready
    func getDriverOrders(completionHandler: @escaping (JSON) -> Void) {
        let path = "api/driver/orders/ready/"
        requestServer(.get, path, nil, URLEncoding.default, completionHandler)
    }
    
    // API - Picking up a ready order
    func pickOrder(orderId: Int, completionHandler: @escaping (JSON) -> Void) {
        let path = "api/driver/order/pick/"
        let params: [String: Any] = [
            "order_id": "\(orderId)",
            "access_token": self.accessToken
        ]
        requestServer(.post, path, params, URLEncoding.default, completionHandler)
    }
    
    // API - Getting the current driver's order
    func getCurrentDriverOrder(completionHandler: @escaping (JSON) -> Void) {
        let path = "api/driver/order/latest/"
        let params: [String: Any] = [
            "access_token": self.accessToken
        ]
        requestServer(.get, path, params, URLEncoding.default, completionHandler)
    }
    
    // API - Updating Driver's location
    func updateLocation(location: CLLocationCoordinate2D, completionHandler: @escaping (JSON) -> Void) {
        let path = "api/driver/location/update/"
        let params: [String: Any] = [
            "access_token": self.accessToken,
            "location": "\(location.latitude), \(location.longitude)"
        ]
        
        requestServer(.post, path, params, URLEncoding.default, completionHandler)
    }
    
    // API - Complete the order
    func completeOrder(orderId : Int, completionHandler: @escaping (JSON) -> Void) {
        let path = "api/driver/order/complete/"
        let params: [String: Any] = [
            "order_id": "\(orderId)",
            "access_token": self.accessToken
        ]
        requestServer(.post, path, params, URLEncoding.default, completionHandler)
    }
    
    // API - Getting Driver's revenue
    
    func getDriverRevenue(completionHandler: @escaping (JSON) -> Void) {
        let path = "api/driver/revenue"
        let params: [String: Any] = [
            "access_token": self.accessToken
        ]
        requestServer(.get, path, params, URLEncoding.default, completionHandler)
    }
    
}
