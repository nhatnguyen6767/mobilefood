//
//  FBManager.swift
//  MobileFood
//
//  Created by net=0 on 21/9/20.
//

import Foundation
import FBSDKLoginKit
import SwiftyJSON

class FBManager {
    
    static let shared = LoginManager()
    
    public class func getFBUserData(completionHandler: @escaping() -> Void) {
        
        if (AccessToken.current != nil) {
            GraphRequest(graphPath: "me", parameters: ["fields":"name, email, picture.type(normal)"]).start { (connection, result, error) in
                
                if (error == nil) {
                    let json = JSON(result!)
                    print(json)
                    
                    User.currentUser.setInfo(json: json)
                    
                    completionHandler()
                }
            }
        }
    }
}
